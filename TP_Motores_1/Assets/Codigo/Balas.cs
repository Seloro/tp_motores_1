using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Balas : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (GameManager.nivel == 2)
        {
            if (other.gameObject.CompareTag("Rojo"))
            {
                Destroy(this.gameObject);
            }
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
}
