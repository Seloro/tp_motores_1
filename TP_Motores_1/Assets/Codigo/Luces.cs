using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Luces : MonoBehaviour
{
    Light luz;
    public GameObject foco;
    public bool sigoRojo;
    GameObject blanco;
    Rigidbody rb;
    public int nivel;
    void Start()
    {
        if (nivel == 3 || nivel == 4)
        {
            luz = GetComponent<Light>();
            luz.enabled = false;
        }
        if (nivel == 3 || nivel == 2)
        {
            if (sigoRojo)
            {
                blanco = GameObject.Find("Rojo");
                rb = blanco.GetComponent<Rigidbody>();
            }
            else
            {
                blanco = GameObject.Find("Azul");
                rb = blanco.GetComponent<Rigidbody>();
            }
        }
    }
    void Update()
    {
        if (nivel == 4)
        {
            if (foco.activeSelf)
            {
                luz.enabled = true;
            }
            else
            {
                luz.enabled = false;
            }
        }
        if (nivel == 3)
        {
            transform.position = new Vector3(blanco.transform.position.x, blanco.transform.position.y + 5.5f, blanco.transform.position.z - 5);
            if (rb.isKinematic)
            {
                luz.enabled = true;
            }
        }
        if (nivel == 2)
        {
            transform.position = new Vector3(blanco.transform.position.x, blanco.transform.position.y + 5, blanco.transform.position.z - 5);
        }
    }
}
