using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReproductorDeMusica : MonoBehaviour
{
    private void Start()
    {
        GestorDeAudio.instancia.PausarSonido("Nivel1");
        GestorDeAudio.instancia.PausarSonido("Nivel2");
        GestorDeAudio.instancia.PausarSonido("Nivel3");
        GestorDeAudio.instancia.PausarSonido("Nivel4");
        GestorDeAudio.instancia.PausarSonido("Menus");
        if (GameManager.nivel == 1 || GameManager.nivel == 2)
        {
            GestorDeAudio.instancia.ReproducirSonido("Nivel1");
        }
        else if (GameManager.nivel == 3 || GameManager.nivel == 4 || GameManager.nivel == 5)
        {
            GestorDeAudio.instancia.ReproducirSonido("Nivel2");
        }
        else if (GameManager.nivel == 6 || GameManager.nivel == 7)
        {
            GestorDeAudio.instancia.ReproducirSonido("Nivel3");
        }
        else if (GameManager.nivel == 8)
        {
            GestorDeAudio.instancia.ReproducirSonido("Nivel4");
        }
        else
        {
            GestorDeAudio.instancia.ReproducirSonido("Menus");
        }
    }
}
