using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollPistonEspecial : MonoBehaviour
{
    Collider col;
    void Start()
    {
        col = GetComponent<Collider>();
    }
    void Update()
    {
        if (transform.position.z <= 47 && transform.position.z >= 13)
        {
            col.enabled = true;
        }
        else
        {
            col.enabled = false;
        }
    }
}
