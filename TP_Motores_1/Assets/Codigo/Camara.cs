using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Camara : MonoBehaviour
{
    GameObject azul;
    GameObject rojo;
    float distancia = 11f;
    Camera cam;
    public GameObject camaraDos;
    private void Start()
    {
        rojo = GameObject.Find("Rojo");
        azul = GameObject.Find("Azul");
        cam = GetComponent<Camera>();
        if (GameManager.nivel == 5)
        {
            transform.localRotation = Quaternion.Euler(30, 0, 0);
            transform.position = new Vector3(0, 6, -45);
        }
        else if (GameManager.nivel == 7)
        {
            transform.localRotation = Quaternion.Euler(45, 0, 0);
            transform.position = new Vector3(0, 10, -7.7f);
        }
        else if (GameManager.nivel == 8)
        {
            transform.localRotation = Quaternion.Euler(20, 0, 0);
            transform.position = new Vector3(0, 8, -26);
        }
    }
    private void Update()
    {
        pocisionamiento();
    }
    private void pocisionamiento()
    {
        if (GameManager.nivel == 1)
        {
            if (azul.transform.position.y >= 5.5f && rojo.transform.position.y >= 5.5f)
            {
                transform.position = new Vector3(0, 14, -2);
                transform.localRotation = Quaternion.Euler(45, 0, 0);
            }
            else
            {
                transform.position = new Vector3(0, 14, -13);
                transform.localRotation = Quaternion.Euler(30, 0, 0);
            }
        }
        if (GameManager.nivel == 4)
        {
            if (math.abs(azul.transform.position.x - rojo.transform.position.x) / 2 <= 10)
            {
                cam.rect = new Rect(0, 0, 1, 1);
                camaraDos.SetActive(false);
                transform.position = new Vector3((math.abs(azul.transform.position.x - rojo.transform.position.x) / 2) + math.min(azul.transform.position.x, rojo.transform.position.x), transform.position.y, transform.position.z);
            }
            else
            {
                cam.rect = new Rect(-0.5f, 0, 1, 1);
                camaraDos.SetActive(true);
                if(azul.transform.position.x > rojo.transform.position.x)
                {
                    transform.position = new Vector3(rojo.transform.position.x, transform.position.y, transform.position.z);
                    camaraDos.transform.position = new Vector3(azul.transform.position.x, transform.position.y, transform.position.z);
                }
                else
                {
                    transform.position = new Vector3(azul.transform.position.x, transform.position.y, transform.position.z);
                    camaraDos.transform.position = new Vector3(rojo.transform.position.x, transform.position.y, transform.position.z);
                }
            }
        }
        if (GameManager.nivel == 5)
        {
            if (azul.transform.position.z >= rojo.transform.position.z)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y, rojo.transform.position.z - distancia);
            }
            else
            {
                transform.position = new Vector3(transform.position.x, transform.position.y, azul.transform.position.z - distancia);
            }
        }
        if (GameManager.nivel == 7)
        {
            if (azul.transform.position.z >= rojo.transform.position.z)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y, rojo.transform.position.z - distancia);
            }
            else
            {
                transform.position = new Vector3(transform.position.x, transform.position.y, azul.transform.position.z - distancia);
            }
        }
    }
}
