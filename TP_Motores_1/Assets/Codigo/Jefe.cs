using System;
using UnityEngine;

public class Jefe : MonoBehaviour
{
    public GameObject nucleo;
    Material material;
    GameObject rojo;
    GameObject azul;
    int miro;
    float temp;
    bool da�o;
    int tempMax;
    static public int vida;
    static public bool escudoActivo;
    public AtaqueJefe ataque;
    static public int fase;
    static public bool miroRojo;
    public GameObject punto1;
    public GameObject punto2;
    public GameObject punto3;
    public GameObject botonEscudo1;
    public Activador botonEscudoCodigo1;
    public GameObject botonEscudo2;
    public Activador botonEscudoCodigo2;
    public GameObject botonEscudo3;
    public Activador botonEscudoCodigo3;
    public GameObject botonEscudo4;
    public Activador botonEscudoCodigo4;
    public Collider botonACol;
    public MeshRenderer botonAMesh;
    public Activador botonACodigo;
    public Collider botonRCol;
    public MeshRenderer botonRMesh;
    public Activador botonRCodigo;
    public GameObject escudo1;
    public GameObject escudo2;
    public GameObject escudo3;
    public GameObject escudo4;
    float tempEscudo;
    public Light luzRoja;
    public Light luzAzul;
    Rigidbody rb;
    public GameObject fuego;
    GameObject fuegoGraficar;
    public GameObject fuegoDos;
    GameObject fuegoGraficarDos;
    private void Awake()
    {
        botonACol.enabled = false;
        botonAMesh.enabled = false;
        botonRCol.enabled = false;
        botonRMesh.enabled = false;
    }
    void Start()
    {
        material = nucleo.GetComponent<MeshRenderer>().material;
        rojo = GameObject.Find("Rojo");
        azul = GameObject.Find("Azul");
        rb = GetComponent<Rigidbody>();
        tempMax = 5;
        vida = 4;
        fase = 0;
    }
    void Update()
    {
        escudoActivo = Convert.ToBoolean(material.GetInt("_EscudoActivo"));
        fases();
        mira();
        escudosActivos();
        esperoDa�o();
        if (fuegoGraficar != null)
        {
            fuegoGraficar.transform.position = transform.position;
        }
        if (fuegoGraficarDos != null)
        {
            fuegoGraficarDos.transform.position = transform.position;
        }
        if (transform.position.y < -10)
        {
            Destroy(fuegoGraficar);
            Destroy(fuegoGraficarDos);
            Destroy(gameObject);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Contrataque"))
        {
            vida--;
            da�o = true;
        }
    }
    void mira()
    {
        if (Convert.ToBoolean(material.GetInt("_EscudoActivo")))
        {
            temp += Time.deltaTime;
            if (temp >= tempMax)
            {
                System.Random randMira = new System.Random();
                miro = randMira.Next(1, 11);
                System.Random randTemp = new System.Random();
                tempMax = randTemp.Next(2, 5);
                temp = 0;
            }
            if (miro <= 5)
            {
                transform.LookAt(rojo.transform);
                miroRojo = true;
            }
            else
            {
                transform.LookAt(azul.transform);
                miroRojo = false;
            }
        }
    }
    void esperoDa�o()
    {
        if (!Convert.ToBoolean(material.GetInt("_EscudoActivo")))
        {
            nucleo.transform.localRotation = Quaternion.Euler(0, -90, -45);
            transform.localRotation = Quaternion.Euler(0, 180, 0);
            botonACol.enabled = true;
            botonAMesh.enabled = true;
            botonRCol.enabled = true;
            botonRMesh.enabled = true;
            luzAzul.enabled = true;
            luzRoja.enabled = true;
            if (da�o)
            {
                nucleo.transform.localRotation = Quaternion.Euler(0, -90, 0);
                botonACodigo.ca�onA = false;
                botonRCodigo.ca�onR = false;
                botonACol.enabled = false;
                botonAMesh.enabled = false;
                botonRCol.enabled = false;
                botonRMesh.enabled = false;
                luzAzul.enabled = false;
                luzRoja.enabled = false;
                if (fase != 4)
                {
                    material.SetInt("_EscudoActivo", 1);
                    if (fase == 1)
                    {
                        punto1.SetActive(true);
                    }
                    else if (fase == 2)
                    {
                        punto2.SetActive(true);
                    }
                    else if(fase == 3)
                    {
                        punto3.SetActive(true);
                        fuegoGraficar = GameObject.Instantiate(fuego, transform.position, fuego.transform.rotation);
                    }
                }
                else
                {
                    rb.isKinematic = false;
                    fuegoGraficarDos = GameObject.Instantiate(fuegoDos, transform.position, fuegoDos.transform.rotation);
                    GestorDeAudio.instancia.ReproducirSonido("Ganar");
                }
                da�o = false;
            }
        }
    }
    void fases()
    {
        if (fase == 0 && ataque.limitaBala == 5)
        {
            material.SetInt("_EscudoActivo", 0);
            fase++;
            ataque.limitaBala = 0;
        }
        if (fase == 1 && ataque.limitaBala == 5)
        {
            material.SetInt("_EscudoActivo", 0);
            fase++;
            ataque.limitaBala = 0;
        }
        if (fase == 2 && ataque.limitaBala == 5)
        {
            material.SetInt("_EscudoActivo", 0);
            fase++;
            ataque.limitaBala = 0;
        }
        if (fase == 3 && ataque.limitaBala == 5)
        {
            material.SetInt("_EscudoActivo", 0);
            fase++;
        }
    }
    void escudosActivos()
    {
        if (Convert.ToBoolean(material.GetInt("_EscudoActivo")))
        {
            tempEscudo = 0;
            if (fase == 0)
            {
                botonEscudo3.SetActive(true);
                botonEscudo4.SetActive(true);
            }
            else if (fase == 1)
            {
                botonEscudo1.SetActive(true);
                botonEscudo2.SetActive(true);
            }
            else if (fase == 2)
            {
                botonEscudo2.SetActive(true);
            }
            else if (fase == 3)
            {
                botonEscudo1.SetActive(true);
            }
        }
        else
        {
            tempEscudo += Time.deltaTime;
            botonEscudo1.SetActive(false);
            botonEscudoCodigo1.escudoA = false;
            botonEscudo2.SetActive(false);
            botonEscudoCodigo2.escudoR = false;
            botonEscudo3.SetActive(false);
            botonEscudoCodigo3.escudoR = false;
            botonEscudo4.SetActive(false);
            botonEscudoCodigo4.escudoA = false;
            if (tempEscudo >= 1)
            {
                escudo1.SetActive(false);
                escudo2.SetActive(false);
                escudo3.SetActive(false);
                escudo4.SetActive(false);
            }
        }
    }
}
