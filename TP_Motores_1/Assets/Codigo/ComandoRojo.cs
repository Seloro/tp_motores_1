using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.ProBuilder;
using UnityEngine.SceneManagement;

public class ComandoRojo : MonoBehaviour
{
    public float velocidad;
    Rigidbody rb;
    bool cresimientoActivo;
    public bool muerto = false;
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        cresimientoActivo = false;
        velocidad = 10;
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S))
        {
            if (Input.GetKey(KeyCode.A))
            {
                rb.AddForce(Vector3.left * velocidad, ForceMode.Force);
            }
            if (Input.GetKey(KeyCode.D))
            {
                rb.AddForce(Vector3.right * velocidad, ForceMode.Force);
            }
            if (Input.GetKey(KeyCode.W))
            {
                rb.AddForce(Vector3.forward * velocidad, ForceMode.Force);
            }
            if (Input.GetKey(KeyCode.S))
            {
                rb.AddForce(Vector3.back * velocidad, ForceMode.Force);
            }
        }
        else
        {
            rb.velocity = new Vector3(0, rb.velocity.y, 0);
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (!cresimientoActivo)
            {
                transform.localScale *= 2;
                cresimientoActivo = true;
                rb.mass *= 10;
                velocidad *= 10;
                GestorDeAudio.instancia.ReproducirSonido("Salto");
            }
            else
            {
                transform.localScale /= 2;
                cresimientoActivo = false;
                rb.mass /= 10;
                velocidad /= 10;
                GestorDeAudio.instancia.ReproducirSonido("Salto");
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Aplastado") || other.gameObject.CompareTag("BalaAzul") || other.gameObject.CompareTag("MatoTodo") || other.gameObject.CompareTag("Turvina") || other.gameObject.CompareTag("ParticulasAzules"))
        {
            muerto = true;
        }
        if (other.gameObject.CompareTag("Puerta"))
        {
            gameObject.SetActive(false);
        }
        if (other.gameObject.CompareTag("Aogado") && !cresimientoActivo)
        {
            muerto = true;
        }
    }
}