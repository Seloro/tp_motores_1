using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor.Rendering;
using UnityEngine;

public class Torretas : MonoBehaviour
{
    public GameObject balaAzul;
    public GameObject balaRoja;
    public bool disparoRojo;
    float temp;
    public float limiteTiempo;
    public int balaV;
    void Start()
    {

    }

    void Update()
    {
        if (disparoRojo)
        {
            if (temp >= limiteTiempo)
            {
                temp = 0;
                GameObject bala;
                bala = Instantiate(balaRoja, transform.position, balaRoja.transform.rotation);
                Rigidbody rb = bala.GetComponent<Rigidbody>();
                rb.AddForce(Vector3.right * balaV, ForceMode.Impulse);
            }
        }
        else
        {
            if (temp >= limiteTiempo)
            {
                temp = 0;
                GameObject bala;
                bala = Instantiate(balaAzul, transform.position, balaAzul.transform.rotation);
                Rigidbody rb = bala.GetComponent<Rigidbody>();
                rb.AddForce(Vector3.right * balaV * -1, ForceMode.Impulse);
            }
        }
        temp += Time.deltaTime;
    }
}
