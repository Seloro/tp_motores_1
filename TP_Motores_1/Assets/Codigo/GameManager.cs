﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public ComandoAzul codigoAzul;
    public GameObject azul;
    public ComandoRojo codigoRojo;
    public GameObject rojo;
    public bool ganan;
    static public int nivel;
    public Rigidbody azulRB;
    public Rigidbody rojoRB;
    float temp;
    int azulVida;
    int rojoVida;
    public Text textoVA;
    public Text textoVR;
    public Text textoVJ;
    public Text textoNivel;
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        nivel = 1;
        rojoVida = 4;
        azulVida = 4;
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            if (nivel < 8)
            {
                ganan = true;
            }
            else
            {
                Destroy(rojo);
                Destroy(azul);
                SceneManager.LoadScene("Ganaste");
                nivel = 0;
                Destroy(gameObject);
            }

        }
        if (Input.GetKeyDown(KeyCode.O))
        {
            azulVida = 4;
            rojoVida = 4;
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            pocisionamiento();
        }
        reinicioPorMuerte();
        condicionParaGanarNivel4y5();
        condicionParaGanarNivel7();
        pasoDeNivel();
        condicionParaFinalizarJuego();
        ui();
        perder();
    }

    private void reinicioPorMuerte()
    {
        if (codigoAzul.muerto || azul.transform.position.y <= -10)
        {
            if (nivel == 1)
            {
                azul.transform.position = new Vector3(2, 0.5f, 0);
                codigoAzul.muerto = false;
            }
            if (nivel == 2)
            {
                azul.transform.position = new Vector3(2, 0.5f, 0);
                codigoAzul.muerto = false;
            }
            if (nivel == 3)
            {
                rojo.transform.position = new Vector3(47.5f, 0.5f, 2.5f);
                codigoAzul.muerto = false;
            }
            if (nivel == 4)
            {
                azul.transform.position = new Vector3(28, 0.5f, 0);
                codigoAzul.muerto = false;
            }
            if (nivel == 5)
            {
                azul.transform.position = new Vector3(2, 0.5f, -34);
                codigoAzul.muerto = false;
            }
            if (nivel == 6)
            {
                azul.transform.position = new Vector3(27, 0.5f, 2);
                codigoAzul.muerto = false;
            }
            if (nivel == 7)
            {
                azul.transform.position = new Vector3(2, 0.5f, 0);
                codigoAzul.muerto = false;
            }
            if (nivel == 8)
            {
                azul.transform.position = new Vector3(2, 1, -15);
                codigoAzul.muerto = false;
            }
            azulVida--;
        }
        if (codigoRojo.muerto || rojo.transform.position.y <= -10)
        {
            if (nivel == 1)
            {
                rojo.transform.position = new Vector3(-2, 0.5f, 0);
                codigoRojo.muerto = false;
            }
            if (nivel == 2)
            {
                rojo.transform.position = new Vector3(-2, 0.5f, 0);
                codigoRojo.muerto = false;
            }
            if (nivel == 3)
            {
                azul.transform.position = new Vector3(2.5f, 0.5f, 2.5f);
                codigoRojo.muerto = false;
            }
            if (nivel == 4)
            {
                rojo.transform.position = new Vector3(24, 0.5f, 0);
                codigoRojo.muerto = false;
            }
            if (nivel == 5)
            {
                rojo.transform.position = new Vector3(-2, 0.5f, -34);
                codigoRojo.muerto = false;
            }
            if (nivel == 6)
            {
                rojo.transform.position = new Vector3(23, 0.5f, 4);
                codigoRojo.muerto = false;
            }
            if (nivel == 7)
            {
                rojo.transform.position = new Vector3(-2, 0.5f, 0);
                codigoRojo.muerto = false;
            }
            if (nivel == 8)
            {
                rojo.transform.position = new Vector3(-2, 1, -15);
                codigoRojo.muerto = false;
            }
            rojoVida--;
        }
    }
    private void pocisionamiento()
    {
        if (nivel == 1)
        {
            azul.transform.position = new Vector3(2, 0.5f, 0);
            rojo.transform.position = new Vector3(-2, 0.5f, 0);
        }
        if (nivel == 2)
        {
            azul.transform.position = new Vector3(2, 0.5f, 2);
            rojo.transform.position = new Vector3(-2, 0.5f, 2);
        }
        if (nivel == 3)
        {
            azul.transform.position = new Vector3(47.5f, 0.5f, 2.5f);
            rojo.transform.position = new Vector3(2.5f, 0.5f, 2.5f);
        }
        if (nivel == 4)
        {
            azul.transform.position = new Vector3(28, 0.5f, 0);
            rojo.transform.position = new Vector3(24, 0.5f, 0);
        }
        if (nivel == 5)
        {
            azul.transform.position = new Vector3(2, 0.5f, -34);
            rojo.transform.position = new Vector3(-2, 0.5f, -34);
        }
        if (nivel == 6)
        {
            azul.transform.position = new Vector3(27, 0.5f, 2);
            rojo.transform.position = new Vector3(23, 0.5f, 2);
        }
        if (nivel == 7)
        {
            azul.transform.position = new Vector3(2, 0.5f, 0);
            rojo.transform.position = new Vector3(-2, 0.5f, 0);
        }
        if (nivel == 8)
        {
            azul.transform.position = new Vector3(2, 1, -15);
            rojo.transform.position = new Vector3(-2, 1, -15);
        }
    }
    private void condicionParaGanarNivel4y5()
    {
        if ((nivel == 4 || nivel == 5) && !azul.gameObject.activeSelf && !rojo.gameObject.activeSelf)
        {
            ganan = true;
            rojo.gameObject.SetActive(true);
            azul.gameObject.SetActive(true);
        }
    }
    private void condicionParaGanarNivel7()
    {
        if (nivel == 7 && Activador.activoR && Activador.activoA)
        {
            azulRB.isKinematic = true;
            rojoRB.isKinematic = true;
            azul.transform.position += transform.up * Time.deltaTime;
            rojo.transform.position += transform.up * Time.deltaTime;
            if (azul.transform.position.y >= 10 && rojo.transform.position.y >= 10)
            {
                ganan = true;
                azulRB.isKinematic = false;
                rojoRB.isKinematic = false;
            }
        }
    }
    private void pasoDeNivel()
    {
        if (ganan)
        {
            ganan = false;
            nivel++;
            SceneManager.LoadScene(nivel);
            pocisionamiento();
        }
    }
    private void condicionParaFinalizarJuego()
    {
        if (nivel == 8)
        {
            if (Jefe.vida == 0)
            {
                if (temp >= 3)
                {
                    Destroy(rojo);
                    Destroy(azul);
                    SceneManager.LoadScene("Ganaste");
                    nivel = 0;
                    Destroy(gameObject);
                }
                temp += Time.deltaTime;
            }
        }
    }
    private void perder()
    {
        if (azulVida == 0 || rojoVida == 0)
        {
            Destroy(rojo);
            Destroy(azul);
            SceneManager.LoadScene("Perdiste");
            nivel = 0;
            Destroy(gameObject);
        }
    }
    private void ui()
    {
        if (azulVida == 4)
        {
            textoVA.text = "Jugador 2: ♥ ♥ ♥ ♥";
        }
        else if (azulVida == 3)
        {
            textoVA.text = "Jugador 2: ♥ ♥ ♥";
        }
        else if (azulVida == 2)
        {
            textoVA.text = "Jugador 2: ♥ ♥";
        }
        else
        {
            textoVA.text = "Jugador 2: ♥";
        }
        if (rojoVida == 4)
        {
            textoVR.text = "Jugador 1: ♥ ♥ ♥ ♥";
        }
        else if (rojoVida == 3)
        {
            textoVR.text = "Jugador 1: ♥ ♥ ♥";
        }
        else if (rojoVida == 2)
        {
            textoVR.text = "Jugador 1: ♥ ♥";
        }
        else
        {
            textoVR.text = "Jugador 1: ♥";
        }
        if (nivel == 8)
        {
            if (Jefe.vida == 4)
            {
                textoVJ.text = "Jefe: ♥ ♥ ♥ ♥";
            }
            else if (Jefe.vida == 3)
            {
                textoVJ.text = "Jefe: ♥ ♥ ♥";
            }
            else if (Jefe.vida == 2)
            {
                textoVJ.text = "Jefe: ♥ ♥";
            }
            else if (Jefe.vida == 1)
            {
                textoVJ.text = "Jefe: ♥";
            }
            else
            {
                textoVJ.text = "Jefe:";
            }
        }
        else
        {
            textoVJ.text = "";
        }
        textoNivel.text = "Nivel: " + nivel;
    }
}
