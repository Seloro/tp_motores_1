using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Activador : MonoBehaviour
{
    static public bool activoR;
    static public bool activoA;
    public bool rojoActiva;
    public GameObject escudo;
    public bool activoEscudo;
    public bool escudoR;
    public bool escudoA;
    public bool ca�onA;
    public bool ca�onR;
    public GameObject llabe;
    public bool turvinaDos;
    static public bool turvinaA1;
    static public bool turvinaA2;
    static public bool turvinaR1;
    static public bool turvinaR2;
    private void Start()
    {
        activoA = false;
        activoR = false;
        escudoA = false;
        escudoR = false;
        ca�onA = false;
        escudoR = false;
        turvinaA1 = false;
        turvinaA2 = false;
        turvinaR1 = false;
        turvinaR2 = false;
    }
    private void Update()
    {
        if (activoEscudo)
        {
            if (rojoActiva)
            {
                escudo.SetActive(escudoR);
            }
            else
            {
                escudo.SetActive(escudoA);
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (rojoActiva)
        {
            if (other.gameObject.CompareTag("Rojo"))
            {
                activoR = true;
                escudoR = true;
                ca�onR =true;
                GestorDeAudio.instancia.ReproducirSonido("Botones");
                if (turvinaDos)
                {
                    turvinaR2 = true;
                }
                else
                {
                    turvinaR1 = true;
                }
            }
        }
        else
        {
            if (other.gameObject.CompareTag("Azul"))
            {
                activoA = true;
                escudoA = true;
                ca�onA = true;
                GestorDeAudio.instancia.ReproducirSonido("Botones");
                if (GameManager.nivel == 2 && llabe != null)
                {
                    llabe.transform.position = new Vector3(-7.5f, 4, 9);
                }
                if (turvinaDos)
                {
                    turvinaA2 = true;
                }
                else
                {
                    turvinaA1 = true;
                }
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (GameManager.nivel == 7)
        {
            if (!rojoActiva)
            {
                if (other.gameObject.CompareTag("Azul") && !activoR)
                {
                    activoA = false;
                }
            }
            else
            {
                if (other.gameObject.CompareTag("Rojo") && !activoA)
                {
                    activoR = false;
                }
            }
        }
        else
        {
            if (!rojoActiva)
            {
                if (other.gameObject.CompareTag("Azul"))
                {
                    activoA = false;
                    escudoA = false;
                    ca�onA = false;
                    if (turvinaDos)
                    {
                        turvinaA2 = false;
                    }
                    else
                    {
                        turvinaA1 = false;
                    }
                }
            }
            else
            {
                if (other.gameObject.CompareTag("Rojo"))
                {
                    activoR = false;
                    escudoR = false;
                    ca�onR = false;
                    if (turvinaDos)
                    {
                        turvinaR2 = false;
                    }
                    else
                    {
                        turvinaR1 = false;
                    }
                }
            }
        }
    }
}