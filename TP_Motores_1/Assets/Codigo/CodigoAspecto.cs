using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CodigoAspecto : MonoBehaviour
{
    public GameObject aspecto;
    static public bool freno;
    void Start()
    {
        freno = false;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Onda"))
        {
            aspecto.GetComponent<Renderer>().material.color = Color.black;
            freno = true;
            gameObject.SetActive(false);
        }
    }
}
