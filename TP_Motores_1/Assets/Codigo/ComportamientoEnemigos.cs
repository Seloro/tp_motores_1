using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComportamientoEnemigos : MonoBehaviour
{
    Rigidbody rb;
    GameObject blanco;
    static public bool sigoRojo;
    public bool rojo;
    public GameObject aspecto;
    float temp;
    bool enbisto;
    void Start()
    {
        System.Random randx = new System.Random();
        System.Random randz = new System.Random();
        transform.position = new Vector3 (randx.Next(2,49), 1.5f, randz.Next(6,49));
        sigoRojo = rojo;
        rb = GetComponent<Rigidbody>();
        if (rojo)
        {
            blanco = GameObject.Find("Rojo");
            aspecto.GetComponent<Renderer>().material.color = Color.blue;
        }
        else
        {
            blanco = GameObject.Find("Azul");
            aspecto.GetComponent<Renderer>().material.color = Color.red;
        }
        enbisto = true;
    }
    void Update()
    {
        if (rojo)
        {
            if (!CodigoAspecto.freno)
            {
                transform.LookAt(blanco.transform);
                if (Physics.Raycast(transform.position, Vector3.down, 0.6f))
                {
                    rb.AddForce(Vector3.up * 2, ForceMode.Impulse);
                    GestorDeAudio.instancia.ReproducirSonido("Salto");
                }
                if (rb.velocity.x <= 60 && rb.velocity.z <= 60)
                {
                    rb.AddForce(transform.forward * 1, ForceMode.Force);
                }
            }
        }
        else if (!rojo)
        {
            if (!CodigoAspecto.freno)
            {
                if (enbisto)
                {
                    transform.LookAt(blanco.transform);
                    if (temp >= 3)
                    {
                        rb.AddForce(transform.forward * 20, ForceMode.Impulse);
                        temp = 0;
                        enbisto = false;
                        GestorDeAudio.instancia.ReproducirSonido("Salto");
                    }
                    temp += Time.deltaTime;
                }
                else
                {
                    if (temp >= 3)
                    {
                        rb.velocity = Vector3.zero;
                        if (temp >= 4)
                        {
                            temp = 0;
                            enbisto = true;
                        }
                    }
                    temp += Time.deltaTime;
                }
            }
        }
    }
}
