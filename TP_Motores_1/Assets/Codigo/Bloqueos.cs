using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bloqueos : MonoBehaviour
{
    public bool rojoActiva;
    bool meMuevo;
    private void Start()
    {
        meMuevo = false;
    }
    void Update()
    {
        if (rojoActiva)
        {
            if (Activador.activoR)
            {
                meMuevo = true;
            }
            if (meMuevo)
            {
                if (transform.position.y <= 7.5f)
                {
                    transform.position += transform.up * Time.deltaTime;
                }
                else if (transform.position.x < 32.5f)
                {
                    transform.position += transform.right * Time.deltaTime;
                }
            }
        }
        else
        {
            if (Activador.activoA)
            {
                meMuevo = true;
            }
            if (meMuevo)
            {
                if (transform.position.y <= 7.5f)
                {
                    transform.position += transform.up * Time.deltaTime;
                }
                else if (transform.position.x < 22.5f)
                {
                    transform.position += transform.right * Time.deltaTime;
                }
            }
        }
    }
}
