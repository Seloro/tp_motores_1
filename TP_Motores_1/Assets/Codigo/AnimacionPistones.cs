using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimacionPistones : MonoBehaviour
{
    public float velocidadAnimacion;
    Animator animacion;
    public float speed;
    public int direccion;
    public bool vertical;
    public bool horizontal;
    public float velocidad;
    private void Start()
    {
        animacion = GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
        if (horizontal)
        {
            GetComponent<Rigidbody>().velocity += new Vector3(velocidad * velocidadAnimacion, 0, 0) * direccion;
        }
        if (vertical)
        {
            GetComponent<Rigidbody>().velocity += new Vector3(0, 0, velocidad * velocidadAnimacion) * direccion;
        }
    }
    private void Update()
    {
        animacion.speed = speed;
    }
}
