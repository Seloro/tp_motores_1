using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Astas : MonoBehaviour
{
    Rigidbody rb;
    public bool cambio;
    public float x;
    public float y;
    public float z;
    public GameObject parRojo;
    public GameObject parAzul;
    public bool rojoCambia;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        parAzul.SetActive(false);
        parRojo.SetActive(false);
    }
    void Update()
    {
        if (rojoCambia)
        {
            if (Activador.activoR)
            {
                cambio = true;
            }
            else
            {
                cambio = false;
            }
        }
        else
        {
            if (Activador.activoA)
            {
                cambio = false;
            }
            else
            {
                cambio = true;
            }
        }
        if (cambio)
        {
            if (z <= 400)
            {
                z += Time.deltaTime * 100;
            }
        }
        else
        {
            if (z >= -400)
            {
                z -= Time.deltaTime * 100;
            }
        }
        if (z > 0)
        {
            parRojo.SetActive(false);
            parAzul.SetActive(true);
        }
        else
        {
            parAzul.SetActive(false);
            parRojo.SetActive(true);
        }
        transform.Rotate(new Vector3(x, y, z) * Time.deltaTime);
    }
}