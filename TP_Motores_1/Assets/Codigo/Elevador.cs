using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using Unity.VisualScripting;
using UnityEngine;

public class Elevador : MonoBehaviour
{
    public BoxCollider aplastador;
    bool baja;
    private void Start()
    {
        if (GameManager.nivel == 1)
        {
            aplastador.enabled = false;
        }

    }
    void Update()
    {
        if (GameManager.nivel == 1)
        {
            if (!Activador.activoA && transform.position.y <= 5.5)
            {
                aplastador.enabled = false;
                transform.position += transform.up * 2 * Time.deltaTime;
            }
            else if (Activador.activoA && transform.position.y >= -0.495f)
            {
                aplastador.enabled = true;
                transform.position -= transform.up * 2 * Time.deltaTime;
            }
        }
        if (GameManager.nivel == 7)
        {
            if (transform.position.y >= 0.5f)
            {
                baja = true;
            }
            else if (transform.position.y <= -2.5f)
            {
                baja = false;
            }
            if (baja)
            {
                transform.position -= transform.up * (Time.deltaTime / 2);
            }
            else
            {
                transform.position += transform.up * (Time.deltaTime / 2);
            }
        }
    }
}
