using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotonEspecialRojo : MonoBehaviour
{
    static public bool botonEspecialRojo;
    Collider col;
    MeshRenderer mr;
    void Start()
    {
        botonEspecialRojo = false;
        col = GetComponent<Collider>();
        mr = GetComponent<MeshRenderer>();
        col.enabled = false;
        mr.enabled = false;
    }
    void Update()
    {
        if (Activador.turvinaA1 && Activador.turvinaR1)
        {
            col.enabled = true;
            mr.enabled = true;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Rojo"))
        {
            botonEspecialRojo = true;
            GestorDeAudio.instancia.ReproducirSonido("Botones");
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Rojo"))
        {
            botonEspecialRojo = false;
        }
    }
}
