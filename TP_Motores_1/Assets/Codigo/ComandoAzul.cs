using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ComandoAzul : MonoBehaviour
{
    public float velocidad;
    Rigidbody rb;
    public float impulso;
    public bool muerto = false;
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        velocidad = 10;
        impulso = 5;
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.DownArrow))
        {
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                rb.AddForce(Vector3.left * velocidad, ForceMode.Force);
            }
            if (Input.GetKey(KeyCode.RightArrow))
            {
                rb.AddForce(Vector3.right * velocidad, ForceMode.Force);
            }
            if (Input.GetKey(KeyCode.UpArrow))
            {
                rb.AddForce(Vector3.forward * velocidad, ForceMode.Force);
            }
            if (Input.GetKey(KeyCode.DownArrow))
            {
                rb.AddForce(Vector3.back * velocidad, ForceMode.Force);
            }
        }
        else
        {
            rb.velocity = new Vector3(0f, rb.velocity.y, 0f);
            transform.localRotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, transform.rotation.z);
        }
        if ((Input.GetKeyDown(KeyCode.Keypad0) || Input.GetKeyDown(KeyCode.Joystick1Button0)) && Physics.Raycast(transform.position, Vector3.down, 0.6f))
        {
            rb.AddForce(Vector3.up * impulso, ForceMode.Impulse);
            GestorDeAudio.instancia.ReproducirSonido("Salto");
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Aplastado") || other.gameObject.CompareTag("BalaRoja") || other.gameObject.CompareTag("MatoTodo") || other.gameObject.CompareTag("Aogado") || other.gameObject.CompareTag("Turvina") || other.gameObject.CompareTag("ParticulasRojas"))
        {
            muerto = true;
        }
        if (other.gameObject.CompareTag("Puerta"))
        {
            gameObject.SetActive(false);
        }
    }
}