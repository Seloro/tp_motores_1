using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class AtaqueJefe : MonoBehaviour
{
    static public bool ataco;
    float tempParaAtacar;
    public GameObject balaMataTodo;
    GameObject rojo;
    GameObject azul;
    int miro;
    float tempMira;
    int tempMaxMira;
    float tempBala;
    public int balaV;
    public int limitaBala;
    public bool centro;
    void Start()
    {
        rojo = GameObject.Find("Rojo");
        azul = GameObject.Find("Azul");
    }
    void Update()
    {
        enfriamiento();
        mira();
        ataque();
    }
    void enfriamiento()
    {
        if (Jefe.escudoActivo)
        {
            tempParaAtacar += Time.deltaTime;
            if (tempParaAtacar >= 2)
            {
                ataco = true;
            }
        }
        else
        {
            ataco = false;
            tempParaAtacar = 0;
        }
    }
    void mira()
    {
        if (!centro)
        {
            if (ataco)
            {
                tempMira += Time.deltaTime;
                if (tempMira >= tempMaxMira)
                {
                    System.Random randMira = new System.Random();
                    miro = randMira.Next(1, 11);
                    System.Random randTemp = new System.Random();
                    tempMaxMira = randTemp.Next(2, 5);
                    tempMira = 0;
                }
                if (miro <= 5)
                {
                    transform.LookAt(rojo.transform);
                }
                else
                {
                    transform.LookAt(azul.transform);
                }
            }
        }
    }
    void ataque()
    {
        if (ataco)
        {
            if (tempBala >= 3)
            {
                tempBala = 0;
                GameObject bala;
                bala = Instantiate(balaMataTodo, transform.position, transform.rotation);
                Rigidbody rb = bala.GetComponent<Rigidbody>();
                rb.AddForce(transform.forward * balaV, ForceMode.Impulse);
                limitaBala++;
                GestorDeAudio.instancia.ReproducirSonido("Disparos");
                Destroy(bala, 5);
            }
            tempBala += Time.deltaTime;
        }
    }
}
