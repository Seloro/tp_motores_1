using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuertaNivelturvinas : MonoBehaviour
{
    bool mover;
    void Start()
    {
        mover = false;
    }
    void Update()
    {
        if (BotonEspecialAzul.botonEspecialAzul && BotonEspecialRojo.botonEspecialRojo)
        {
            mover = true;
        }
        if (mover)
        {
            if (transform.position.y <= 9)
            {
                transform.position += transform.up * Time.deltaTime;
            }
        }
    }
}
