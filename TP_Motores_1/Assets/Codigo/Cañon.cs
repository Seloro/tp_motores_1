using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cañon : MonoBehaviour
{
    public GameObject cañon;
    GameObject jefe;
    public GameObject balaPrefab;
    public float balaV;
    float temp;
    bool puedoDisparar;
    public Activador activador1;
    public Activador activador2;
    void Start()
    {
        jefe = GameObject.Find("Jefe");
    }
    void Update()
    {
        if (jefe != null)
        {
            transform.LookAt(jefe.transform);
        }
        disparo();
        movimiento();
    }
    void disparo()
    {
        if (cañon.transform.position.y >= -2)
        {
            temp += Time.deltaTime;
            if (temp >= 2 && puedoDisparar)
            {
                GameObject bala;
                bala = Instantiate(balaPrefab, transform.position, transform.rotation);
                Rigidbody rb = bala.GetComponent<Rigidbody>();
                rb.AddForce((transform.forward + transform.up) * balaV, ForceMode.Impulse);
                GestorDeAudio.instancia.ReproducirSonido("Disparos");
                puedoDisparar = false;
            }
        }
        else
        {
            temp = 0;
            puedoDisparar= true;
        }
    }
    void movimiento()
    {
        if (cañon.transform.position.y >= -15 && !activador1.cañonA && !activador2.cañonR)
        {
            cañon.transform.position -= transform.up * 2 * Time.deltaTime;
        }
        else if (cañon.transform.position.y <= -2 && activador1.cañonA && activador2.cañonR)
        {
            cañon.transform.position += transform.up * 2 * Time.deltaTime;
        }
    }
}
