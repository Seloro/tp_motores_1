using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class Meta : MonoBehaviour
{
    BoxCollider col;
    MeshRenderer mr;
    public Light luz;
    static public bool ganan;
    public LayerMask azulEsta;
    public LayerMask rojoEsta;
    float temp = 0;
    GameObject gameManager;
    GameManager manager;
    bool pausa;
    void Start()
    {
        gameManager = GameObject.Find("GameManager");
        mr = GetComponent<MeshRenderer>();
        col = GetComponent<BoxCollider>();
        manager = gameManager.GetComponent<GameManager>();
        mr.enabled = false;
        col.enabled = false;
        luz.enabled = false;
    }
    void Update()
    {
            if ((Activador.activoR || Onda.contador == 4) && !mr.enabled)
            {
                mr.enabled = true;
                col.enabled = true;
                luz.enabled = true;
                pausa = true;
            }

            if (rojoEnContacto() && azulEnContacto() && pausa)
            {
                if (temp >= 2)
                {
                    manager.ganan = true;
                    temp = 0;
                    pausa = false;
                }
                temp += Time.deltaTime;
            }
            else
            {
                temp = 0;
            }
    }
    private bool rojoEnContacto()
    {
        return Physics.CheckBox(col.bounds.center, transform.localScale / 2, transform.rotation, rojoEsta);
    }
    private bool azulEnContacto()
    {
        return Physics.CheckBox(col.bounds.center, transform.localScale / 2, transform.rotation, azulEsta);
    }
}
