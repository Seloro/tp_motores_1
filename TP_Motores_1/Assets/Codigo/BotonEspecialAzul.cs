using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotonEspecialAzul : MonoBehaviour
{
    static public bool botonEspecialAzul;
    Collider col;
    MeshRenderer mr;
    void Start()
    {
        botonEspecialAzul = false;
        col = GetComponent<Collider>();
        mr = GetComponent<MeshRenderer>();
        col.enabled = false;
        mr.enabled = false;
    }
    void Update()
    {
        if(Activador.turvinaA2 && Activador.turvinaR2)
        {
            col.enabled = true;
            mr.enabled = true;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Azul"))
        {
            botonEspecialAzul = true;
            GestorDeAudio.instancia.ReproducirSonido("Botones");
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Azul"))
        {
            botonEspecialAzul = false;
        }
    }
}
