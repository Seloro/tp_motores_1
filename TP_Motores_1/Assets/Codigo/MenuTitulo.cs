using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuTitulo : MonoBehaviour
{
    public void jugar()
    {
        SceneManager.LoadScene(1);
    }
    public void salir()
    {
        Application.Quit();
    }
    public void volver()
    {
        SceneManager.LoadScene(0);
    }
}
