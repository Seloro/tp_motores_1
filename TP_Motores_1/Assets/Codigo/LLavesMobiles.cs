using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class LLavesMobiles : MonoBehaviour
{
    bool meMuevo;
    public GameObject cuerpo;
    GameObject blanco;
    public bool rojo;
    public bool llaveAzul;
    public bool llaveRoja;
    Rigidbody rb;
    void Start()
    {
        System.Random randx = new System.Random();
        System.Random randz = new System.Random();
        cuerpo.transform.position = new Vector3(randx.Next(2, 49), 1.5f, randz.Next(20, 49));
        meMuevo = true;
        if (rojo)
        {
            blanco = GameObject.Find("Rojo");
            cuerpo.GetComponent<Renderer>().material.color = Color.red;
        }
        else
        {
            blanco = GameObject.Find("Azul");
            cuerpo.GetComponent<Renderer>().material.color = Color.blue;
        }
        llaveAzul = false;
        llaveRoja = false;
        rb = cuerpo.GetComponent<Rigidbody>();
    }
    void Update()
    {
        if (meMuevo)
        {
            cuerpo.transform.LookAt(blanco.transform);
            if (rb.velocity.magnitude <= 20)
            {
                rb.AddForce(transform.forward * -50, ForceMode.Force);
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (rojo)
        {
            if (other.gameObject.CompareTag("Rojo") && meMuevo)
            {
                llaveRoja = true;
                meMuevo = false;
                GestorDeAudio.instancia.ReproducirSonido("Botones");
            }
        }
        else
        {
            if (other.gameObject.CompareTag("Azul") && meMuevo)
            {
                llaveAzul = true;
                meMuevo = false;
                GestorDeAudio.instancia.ReproducirSonido("Botones");
            }
        }
    }
}
