using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Onda : MonoBehaviour
{
    public LLavesMobiles llaveAzul;
    public LLavesMobiles llaveRoja;
    static public int contador;
    void Start()
    {
        contador = 0;
    }
    void Update()
    {
        if (llaveAzul.llaveAzul && llaveRoja.llaveRoja && transform.position.z >= -10)
        {
            transform.position -= transform.forward * 4 * Time.deltaTime;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("BalaRoja") || other.gameObject.CompareTag("BalaAzul"))
        {
            contador++;
        }
    }
}
