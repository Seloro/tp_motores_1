using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;

public class Camara2 : MonoBehaviour
{
    GameObject azul;
    GameObject rojo;
    public GameObject camaraDos;
    Camera cam;
    public float y;
    public float z;
    void Start()
    {
        cam = GetComponent<Camera>();
        rojo = GameObject.Find("Rojo");
        azul = GameObject.Find("Azul");
    }
    void Update()
    {
        if (math.abs(azul.transform.position.x - rojo.transform.position.x) / 2 <= 10 && math.abs(azul.transform.position.z - rojo.transform.position.z) / 2 <= 10)
        {
            cam.rect = new Rect(0, 0, 1, 1);
            camaraDos.SetActive(false);
            transform.position = new Vector3((math.abs(azul.transform.position.x - rojo.transform.position.x) / 2) + math.min(azul.transform.position.x, rojo.transform.position.x), (math.abs(azul.transform.position.y - rojo.transform.position.y) / 2) + math.min(azul.transform.position.y, rojo.transform.position.y) + y, (math.abs(azul.transform.position.z - rojo.transform.position.z) / 2) + math.min(azul.transform.position.z, rojo.transform.position.z) - z);
        }
        else
        {
            cam.rect = new Rect(-0.5f, 0, 1, 1);
            camaraDos.SetActive(true);
            if (azul.transform.position.x > rojo.transform.position.x)
            {
                transform.position = new Vector3(rojo.transform.position.x, rojo.transform.position.y + y, rojo.transform.position.z - z);
                camaraDos.transform.position = new Vector3(azul.transform.position.x, azul.transform.position.y + y, azul.transform.position.z - z);
            }
            else
            {
                transform.position = new Vector3(azul.transform.position.x, azul.transform.position.y + y, azul.transform.position.z - z);
                camaraDos.transform.position = new Vector3(rojo.transform.position.x, rojo.transform.position.y + y, rojo.transform.position.z - z);
            }
        }
    }
}
